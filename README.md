# Gesture Reproducer

An application that enables the use of an Astra SDK compatible camera to record and reproduce time series of human body movement on the computer screen.